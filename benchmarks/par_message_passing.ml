(* SPDX-License-Identifier: MIT *)
module Ref_config = Ref.Config
module Ref = Ref_config.Ref

(** {2. Benchmark parameters} *)

type cfg = {
  batch_size : int;
  niters : int;
  order : order;
  scheduler : scheduler;
}

and order =
  | LIFO
  | FIFO
  (* We could implement a [Rand] method easily with Dynarray,
     but Dynarray is not available on OCaml 4.x. *)

and scheduler =
  | Sequential (* interleaved *)
  | Parallel


(** {2. Benchmark logic} *)

let produce_one () =
  Ref.create (ref ())

let consume_one r =
  let () = Sys.opaque_identity (!(Ref.get r)) in
  Ref.delete r

let produce_batch cfg =
  Array.init cfg.batch_size (fun _ -> produce_one ())

let consume_batch _cfg batch =
  Array.iter consume_one batch

let schedule cfg ~produce ~consume =
  match cfg.scheduler with
  | Sequential ->
    for _ = 1 to cfg.niters do
      produce cfg;
      consume cfg;
    done
  | Parallel ->
    let producer () =
      for _ = 1 to cfg.niters do produce cfg done
    in
    let consumer () =
      for _ = 1 to cfg.niters do consume cfg done
    in
    let producer_dom = Domain.spawn producer in
    consumer ();
    Domain.join producer_dom

type 'a concurrent_bag = {
  push : 'a -> unit;
  pop : unit -> 'a; (** @raise Not_found *)
}

let num_underflows = ref 0
let max_counter = ref 0

let make_bag (type a) ~order : a concurrent_bag =
  let mutex = Mutex.create () in
  let protect_mutex (type b) (f : unit -> b) : b =
    (* no Mutex.protect in OCaml 4.x *)
    Mutex.lock mutex;
    Fun.protect ~finally:(fun () -> Mutex.unlock mutex) f
  in
  let count = Semaphore.Counting.make 0 in
  let protect_producer (f : a -> unit) (v : a) : unit =
    protect_mutex (fun () -> f v);
    Semaphore.Counting.release count;
  in
  let protect_consumer (f : unit -> a) () : a =
    let counter = Semaphore.Counting.get_value count in
    if counter = 0 then incr num_underflows;
    max_counter := max !max_counter counter;
    Semaphore.Counting.acquire count;
    protect_mutex f
  in
  match order with
  | LIFO ->
    let stack = Stack.create () in
    let push v = protect_producer (fun v -> Stack.push v stack) v in
    let pop () = protect_consumer (fun () -> Stack.pop stack) () in
    { push; pop; }
  | FIFO ->
    let queue = Queue.create () in
    let push v = protect_producer (fun v -> Queue.push v queue) v in
    let pop () = protect_consumer (fun () -> Queue.pop queue) () in
    { push; pop; }

let run cfg : unit =
  let bag = make_bag ~order:cfg.order in
  schedule cfg
    ~produce:(fun cfg -> bag.push (produce_batch cfg))
    ~consume:(fun cfg -> consume_batch cfg (bag.pop ()))

(** {2. Parameter parsing} *)
let get_int_var var =
  try int_of_string (Sys.getenv var)
  with _ ->
    Printf.ksprintf failwith
      "We expected an environment variable %s with an integer value."
      var

let batch_size = get_int_var "BATCH_SIZE"
let niters = get_int_var "NITERS"

let get_assoc_var var assoc =
  try List.assoc (Sys.getenv var) assoc
  with _ ->
    Printf.ksprintf failwith
      "We expected an environment variable %s with a value among: %s."
      var
      (List.map fst assoc |> String.concat ", ")

let order = get_assoc_var "ORDER" [
  ("LIFO", LIFO);
  ("FIFO", FIFO);
]

let scheduler = get_assoc_var "SCHEDULER" [
  ("Sequential", Sequential);
  ("Parallel", Parallel);
]

let cfg = {
  niters;
  batch_size;
  order;
  scheduler;
}

(** {2. Init} *)

let () =
  Ref.setup ();
  Printf.printf "%-*s NITERS=%-6d BATCH_SIZE=%-6d: %!"
    Ref_config.max_implem_name_length Ref_config.implem_name cfg.niters cfg.batch_size;
  let before = Time.time () in
  run cfg;
  let after = Time.time () in
  let diff = after -. before in
  Printf.printf "%.2fs (%.2E s/root)\n%!"
    diff
    (diff /. float cfg.niters /. float cfg.batch_size);
  if Ref_config.show_stats then
    Ref.print_stats ();
  Ref.teardown ();
  Printf.printf "num_underflows: %d\nmax_counter: %d\n"
    !num_underflows !max_counter;
