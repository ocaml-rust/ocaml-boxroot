/* SPDX-License-Identifier: MIT */
#include "../../boxroot/boxroot.h"

#include <errno.h>
#include <stdio.h>

#define BOXROOT_HAS_DIAGNOSTIC
void boxroot_diagnostic(void) {
  switch (boxroot_status()) {
    case BOXROOT_NOT_SETUP:
      printf("boxroot fatal error: NOT_SETUP\n");
      break;
    case BOXROOT_TORE_DOWN:
      printf("boxroot fatal error: TORE_DOWN\n");
      break;
    case BOXROOT_INVALID:
      printf("boxroot fatal error: INVALID\n");
      break;
    case BOXROOT_RUNNING:
      switch (errno) {
        case EPERM:
          printf("boxroot fatal error: domain lock not held\n");
          break;
        case ENOMEM:
          printf("boxroot fatal error: out of memory\n");
          break;
        default:
          printf("boxroot fatal error (RUNNING, unknown errno)\n");
          break;
      }
      break;
    default:
      printf("boxroot fatal error (unknown status)\n");
      break;
  }
}

#define MY_PREFIX /* empty string */
#include "gen_boxroot.h"
