/**************************************************************************/
/*                                                                        */
/*                                 OCaml                                  */
/*                                                                        */
/*            Xavier Leroy, projet Cristal, INRIA Rocquencourt            */
/*                                                                        */
/*   Copyright 2001 Institut National de Recherche en Informatique et     */
/*     en Automatique.                                                    */
/*                                                                        */
/*   All rights reserved.  This file is distributed under the terms of    */
/*   the GNU Lesser General Public License version 2.1, with the          */
/*   special exception on linking described in the file LICENSE.          */
/*                                                                        */
/**************************************************************************/

/* Registration of global memory roots */

#ifndef BXR_GLOBROOTS_H
#define BXR_GLOBROOTS_H

#include <caml/mlvalues.h>

CAMLextern void bxr_register_global_root (value *);
CAMLextern void bxr_remove_global_root (value *);
CAMLextern void bxr_modify_global_root(value *r, value newval);

/* The behaviour of the above functions is well-defined only after the
   allocator has been initialised with `dll_boxroot_setup`, which must be
   called after OCaml startup, and before it has released its
   resources with `dll_boxroot_teardown`. */
int bxr_global_roots_setup();
void bxr_global_roots_teardown();

#endif /* BXR_GLOBROOTS_H */
