(* SPDX-License-Identifier: MIT *)
type 'a t = { mutable contents: 'a }

let create v = { contents = v }

let get r = r.contents

let modify a i v = a.(i).contents <- v

let delete r = r.contents <- Obj.magic ()

let setup () = ()
let teardown () = ()
let print_stats () = ()
