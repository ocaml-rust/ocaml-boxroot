/* SPDX-License-Identifier: LGPL-2.1-only WITH OCaml-LGPL-linking-exception

   This file is derived from work by contributors to the OCaml runtime system,
   copyright by Institut National de Recherche en Informatique en Automatique.
*/

#define CAML_INTERNALS

/* Lock-free registration of global (generational) memory roots.

   The code below is adapted from the lockfree-skiplist implementation
   of globroots.c (in the OCaml compiler distribution) which was
   proposed in PR #11826, but not merged upstream due to disappointing
   single-core performance.

   https://github.com/ocaml/ocaml/pull/11826
 */

#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/roots.h>
#include "lf_globroots.h"

#include "../../boxroot/platform.h" // defines OCAML_MULTICORE and bxr_mutex_lock
#if !OCAML_MULTICORE
  #include <caml/misc.h> // caml_fatal_error

  static void unsupported() {
   caml_fatal_error("lf_generational_roots is only available on OCaml 5.x");
  }

  CAMLextern void bxr_register_global_root (value *v) {
    (void)v;
    unsupported();
  }

  CAMLextern void bxr_remove_global_root (value *r) {
    (void)r;
    unsupported();
  }

  CAMLextern void bxr_modify_global_root(value *r, value newval) {
    (void)r; (void)newval;
    unsupported();
  }

  int bxr_global_roots_setup() {
    unsupported();
  }
  void bxr_global_roots_teardown() {
    unsupported();
  }
#else
#include <caml/lf_skiplist.h>

/* The two global root lists.
   Each is represented by a skip list with the key being the address
   of the root.  (The associated data field is unused.) */

struct lf_skiplist bxr_global_roots_young;
                  /* generational roots pointing to minor or major heap */
struct lf_skiplist bxr_global_roots_old;
                  /* generational roots pointing to major heap */

/* The invariant of the generational roots is the following:
   - If the global root contains a pointer to the minor heap, then the root is
     in [bxr_global_roots_young];
   - If the global root contains a pointer to the major heap, then the root is
     in [bxr_global_roots_old] or in [bxr_global_roots_young];
   - Otherwise (the root contains a pointer outside of the heap or an integer),
     then neither [bxr_global_roots_young] nor [bxr_global_roots_old] contain
     it. */

/* Free a skiplist. This is not concurrency-safe, it must be calld from
   a single thread at a time when there can be no concurrent access to this
   skiplist */
static void caml_lf_skiplist_free(struct lf_skiplist *sk) {
  struct lf_skipcell *curr, *next;
  caml_lf_skiplist_free_garbage(sk);
  curr = (sk)->head->forward[0];
  while (curr != (sk)->tail) {
    int marked;
    LF_SK_EXTRACT(curr->forward[0], marked, next);
    (void)marked;
    caml_stat_free(curr);
    curr = next;
  }
  caml_stat_free(sk->head);
  caml_stat_free(sk->tail);
}

void bxr_init_global_roots()
{
  caml_lf_skiplist_init(&bxr_global_roots_young);
  caml_lf_skiplist_init(&bxr_global_roots_old);
}

void bxr_free_global_roots()
{
  caml_lf_skiplist_free(&bxr_global_roots_young);
  caml_lf_skiplist_free(&bxr_global_roots_old);
}

/* Insertion and deletion */

Caml_inline void bxr_insert_global_root(struct lf_skiplist * list, value * r)
{
  caml_lf_skiplist_insert(list, (uintnat) r, 0);
}

Caml_inline void bxr_delete_global_root(struct lf_skiplist * list, value * r)
{
  caml_lf_skiplist_remove(list, (uintnat) r);
}

/* generational status of roots */

enum gc_root_class {
  YOUNG,
  OLD,
  UNTRACKED
};

static enum gc_root_class classify_gc_root(value v)
{
  if(!Is_block(v)) return UNTRACKED;
  if(Is_young(v)) return YOUNG;
  return OLD;
}

/* Register a global C root of the generational kind */

CAMLexport void bxr_register_global_root(value *r)
{
  Caml_check_caml_state();
  CAMLassert (((intnat) r & 3) == 0);  /* compact.c demands this (for now) */

  switch(classify_gc_root(*r)) {
    case YOUNG:
      bxr_insert_global_root(&bxr_global_roots_young, r);
      break;
    case OLD:
      bxr_insert_global_root(&bxr_global_roots_old, r);
      break;
    case UNTRACKED: break;
  }
}

/* Un-register a global C root of the generational kind */

CAMLexport void bxr_remove_global_root(value *r)
{
  switch(classify_gc_root(*r)) {
    case OLD:
      bxr_delete_global_root(&bxr_global_roots_old, r);
      /* Fallthrough: the root can be in the young list while actually
         being in the major heap. */
    case YOUNG:
      bxr_delete_global_root(&bxr_global_roots_young, r);
      break;
    case UNTRACKED: break;
  }
}

/* Modify the value of a global C root of the generational kind */

CAMLexport void bxr_modify_global_root(value *r, value newval)
{
  enum gc_root_class c;
  /* See PRs #4704, #607 and #8656 */
  switch(classify_gc_root(newval)) {
    case YOUNG:
      c = classify_gc_root(*r);
      if(c == OLD)
        bxr_delete_global_root(&bxr_global_roots_old, r);
      if(c != YOUNG)
        bxr_insert_global_root(&bxr_global_roots_young, r);
      break;

    case OLD:
      /* If the old class is YOUNG, then we do not need to do
         anything: It is OK to have a root in roots_young that
         suddenly points to the old generation -- the next minor GC
         will take care of that. */
      if(classify_gc_root(*r) == UNTRACKED)
        bxr_insert_global_root(&bxr_global_roots_old, r);
      break;

    case UNTRACKED:
      bxr_remove_global_root(r);
      break;
  }

  *r = newval;
}

/* Iterate a GC scanning action over a global root list */
Caml_inline void bxr_iterate_global_roots(scanning_action f,
                                      struct lf_skiplist * rootlist, void* fdata)
{
  FOREACH_LF_SKIPLIST_ELEMENT(e, rootlist, {
      value * r = (value *) (e->key);
      f(fdata, *r, r);
    })
}

/* The in-runtime implementation of scanning functions are STW-protected,
   but scanning hooks may be called concurrently so we use a mutex.
   It only protects between collector/collector races, we know that
   all mutators are stopped by the STW section at this point.
*/
static mutex_t roots_mutex = BXR_MUTEX_INITIALIZER;

/* Scan all global roots. */
void bxr_scan_global_roots(scanning_action f, void* fdata) {
  bxr_mutex_lock(&roots_mutex);
  bxr_iterate_global_roots(f, &bxr_global_roots_young, fdata);
  caml_lf_skiplist_free_garbage(&bxr_global_roots_young);

  bxr_iterate_global_roots(f, &bxr_global_roots_old, fdata);
  caml_lf_skiplist_free_garbage(&bxr_global_roots_old);
  bxr_mutex_unlock(&roots_mutex);
}

/* Scan global roots for a minor collection. */
void bxr_scan_global_young_roots(scanning_action f, void* fdata)
{
  bxr_mutex_lock(&roots_mutex);
  /* Note: we do not free the skiplist garbage here,
     only in [bxr_scan_global_roots], to reduce
     minor GC latency. */
  bxr_iterate_global_roots(f, &bxr_global_roots_young, fdata);

  /* Move young roots to old roots */
  int young_count = 0;
  FOREACH_LF_SKIPLIST_ELEMENT(e, &bxr_global_roots_young, {
      young_count++;
      value * r = (value *) (e->key);
      caml_lf_skiplist_insert(&bxr_global_roots_old, (uintnat) r, 0);
    });
  if (young_count > 0) {
    caml_lf_skiplist_free(&bxr_global_roots_young);
    caml_lf_skiplist_init(&bxr_global_roots_young);
  }
  bxr_mutex_unlock(&roots_mutex);
}

/* Register our scanning functions with GC hooks */

#include "../../boxroot/ocaml_hooks.h"

static void scanning_callback(scanning_action action, int only_young, void *data)
{
  (void)only_young;
  int in_minor_collection = bxr_in_minor_collection();
  if (in_minor_collection) {
    bxr_scan_global_young_roots(action, data);
  } else {
    bxr_scan_global_roots(action, data);
  }
  return;
}

_Atomic static int setup = 0;
static mutex_t setup_mutex = BXR_MUTEX_INITIALIZER;

int bxr_global_roots_setup()
{
  if (setup) return 0;
  bxr_mutex_lock(&setup_mutex);
  {
    if (setup) {
      bxr_mutex_unlock(&setup_mutex);
      return 0;
    }
    bxr_init_global_roots();
    bxr_setup_hooks(&scanning_callback, NULL);
  }
  bxr_mutex_unlock(&setup_mutex);
  return 1;
}

void bxr_global_roots_teardown()
{
  if (!setup) return;
  bxr_mutex_lock(&setup_mutex);
  {
    if (!setup) {
      bxr_mutex_unlock(&setup_mutex);
      return;
    }
    /* bxr_free_global_roots(); */
  }
  bxr_mutex_unlock(&setup_mutex);
  return;
}

#endif // OCAML_MULTICORE
