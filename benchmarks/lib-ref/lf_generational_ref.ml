(* SPDX-License-Identifier: MIT *)
type 'a t
external create : 'a -> 'a t         = "lf_generational_ref_create"
external get : 'a t -> 'a            = "lf_generational_ref_get"
external modify : 'a t array -> int -> 'a -> unit = "lf_generational_ref_modify"
external delete : 'a t -> unit       = "lf_generational_ref_delete"

external setup : unit -> unit        = "lf_generational_setup"
external teardown : unit -> unit     = "lf_generational_teardown"

let print_stats () = ()
