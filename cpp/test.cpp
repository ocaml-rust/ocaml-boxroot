/* SPDX-License-Identifier: MIT */

/* Demo of Boxroot smart pointer for C++ */

#include "cpp/boxroot.h"

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/callback.h>

#include <stdexcept>
#include <iostream>

static Boxroot cons(Boxroot hd, Boxroot tl)
{
  Boxroot cell(caml_alloc_small(2, Tag_cons));
  Field(*cell, 0) = *hd;
  Field(*cell, 1) = *tl;
  return cell;
}

// should be tail recursive (it is not, neither with g++ nor clang)
static Boxroot make_list_rec(int n, Boxroot res)
{
  if (n == 0) return res;
  else {
    n = n - 1;
    Boxroot cell = cons(Boxroot(Val_int(n)), std::move(res));
    return make_list_rec(n, std::move(cell));
  }
}

static Boxroot make_list(int n)
{
  return make_list_rec(n, Boxroot(Val_emptylist));
}

/*
Boxroot make_list(int n)
{
  Boxroot l(Val_emptylist);
  for (int i = n - 1; i >= 0; i--) {
    l = cons(Boxroot(Val_int(i)), std::move(l));
  }
  return l;
}
*/

// should be tail recursive (it is not, neither with g++ nor clang)
static int list_length_rec(Boxroot l, int n)
{
  if (*l == Val_emptylist)
    return n;
  else {
    if (Tag_val(*l) != Tag_cons) throw std::invalid_argument("Not a list");
    value tl = Field(*l, 1);
    l.set(tl);
    return list_length_rec(std::move(l), n+1);
  }
}

static int list_length(Boxroot l)
{
  return list_length_rec(std::move(l), 0);
}

/*
int list_length(Boxroot l)
{
  int n = 0;
  while (*l != Val_emptylist) {
    if (Tag_val(*l) != Tag_cons) throw std::invalid_argument("Not a list");
    l.set(Field(*l, 1));
    n++;
  }
  return n;
}
*/

static const int num = 1000000;

static void test1()
{
  Boxroot list = make_list(num);
  int len = list_length(Boxroot(*list));
  if (len != num) throw std::logic_error("length mismatch");
}

int main(int, char ** argv)
{
  caml_startup(argv);
  test1();
  caml_shutdown();
  std::cout << "Test complete" << std::endl;
  return 0;
}
